export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
export ALRB_localConfigDir=$HOME/localConfig
setupATLAS
lsetup panda rucio emi prmon
voms-proxy-init --valid 96:00 #--voms atlas:/atlas/Role=production
