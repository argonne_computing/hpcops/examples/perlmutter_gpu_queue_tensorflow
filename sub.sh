#!/bin/bash
#SBATCH -A m2616_g
#SBATCH -C gpu
#SBATCH --qos=debug
#SBATCH --time 00:10:00
#SBATCH --module=cvmfs
##SBATCH -L SCRATCH,project
#SBATCH -N 1
#SBATCH -G 2
#SBATCH -c 1

# to run: sbatch sub.sh
. ./run.sh # standelone