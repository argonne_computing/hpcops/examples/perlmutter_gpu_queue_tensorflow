#!/bin/bash

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase;

####### shifter ############
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -c centos7 --swtype shifter;
if [ $(env | grep "SHIFTER_RUNTIME") == 'SHIFTER_RUNTIME=1' ]
    then echo "Running in container"
else
    echo "Error not in container...Exit..."
    # exit
fi

######  singularity ########
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -c centos7 -r run_img.sh

