#!/bin/bash

# image='atlas/athena:21.0.15.sw6-0'
image='qianrong/ml-base'

if [ $(shifterimg images|grep -c -e $image) == 0 ]
    then 
        echo "Pull image $image ..."
        shifterimg pull $image
else  echo "Image $image found in shifter hub"
fi

shifter --image=$image -m cvmfs,gpu -- /bin/bash run_img.sh
