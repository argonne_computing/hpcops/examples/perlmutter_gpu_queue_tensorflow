#!/usr/env/python

import tensorflow as tf
import keras
import numpy as np
import os, sys

def main():

  print("TensorFlow version:", tf.__version__)

  tf.debugging.set_log_device_placement(True)

  ngpu = len(tf.config.list_physical_devices('GPU'))
  print("Num GPUs Available: ", ngpu )

  if ngpu:
    print('GPU found')
  else:
    print("No GPU found")
    sys.exit(2)

  # mnist = tf.keras.datasets.mnist

  # (x_train, y_train), (x_test, y_test) = mnist.load_data()
  origin_folder = 'http://storage.googleapis.com/tensorflow/tf-keras-datasets/'
  path = keras.utils.data_utils.get_file(
      'mnist.npz',
      origin=origin_folder + 'mnist.npz',
      file_hash=
      '731c5ac602752760c8e48fbffcf8c3b850d9dc2a2aedcf2cc48468fc17b673d1')

  with np.load(path, allow_pickle=True) as f:  # pylint: disable=unexpected-keyword-arg
    x_train, y_train = f['x_train'], f['y_train']
    x_test, y_test = f['x_test'], f['y_test']
  
  x_train, x_test = x_train / 255.0, x_test / 255.0

  model = tf.keras.models.Sequential([
    tf.keras.layers.Flatten(input_shape=(28, 28)),
    tf.keras.layers.Dense(128, activation='relu'),
    tf.keras.layers.Dropout(0.2),
    tf.keras.layers.Dense(10)
  ])

  predictions = model(x_train[:1]).numpy()
  print(predictions)

  tf.nn.softmax(predictions).numpy()

  loss_fn = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
  loss_fn(y_train[:1], predictions).numpy()

  model.compile(optimizer='adam',
                loss=loss_fn,
                metrics=['accuracy'])

  model.fit(x_train, y_train, epochs=5)

  model.evaluate(x_test,  y_test, verbose=2)

  probability_model = tf.keras.Sequential([
    model,
    tf.keras.layers.Softmax()
  ])

  print (probability_model(x_test[:5]))
  model.save("my_model.h5")

if __name__ == "__main__":
  main()
