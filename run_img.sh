#!/bin/bash
`which nvidia-smi` 
nvidia-smi 

if [ $(env | grep "SHIFTER_RUNTIME") == 'SHIFTER_RUNTIME=1' ]
    then echo "Running in container"
else
    echo "Error not in container...Exit..."
    # exit
fi

local_sitepackage_path=`python -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())"`

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase;
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh;
# asetup Athena,master,latest

# lsetup "views LCG_102b_cuda x86_64-centos7-gcc8-opt" prmon

# setting these after the ALRB setup so they are picked over ALRB packages
PYTHONPATH=$local_sitepackage_path
echo $PYTHONPATH

python -m pip list

python -c "import graph_nets,numpy;print(graph_nets.print_function);print(numpy.__version__)"
exeStat=$?
# exit $exeStat
