#!/bin/bash

# prun --exec="./run.sh" --inDS hc_test:mc15_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.evgen.EVNT.e3601_tid04972714_00  --nFiles 1 --outDS user.$USER.tensorflow.`uuidgen` --outputs my_model.h5 --disableAutoRetry --noBuild  --extFile=*.log,*.h5,prmon*  --site NERSC_Perlmutter_GPU --nCore=4 --architecture nvidia # --prodSourceLabel test

## custom image
prun --exec="./run_custom_img.sh" --outDS user.$USER.tensorflow.`uuidgen` --outputs my_model.h5 --disableAutoRetry --noBuild  --extFile=*.log,*.h5,prmon*  --site NERSC_Perlmutter_GPU --nCore=4 --architecture nvidia

#ANALY_BNL_GPU_ARC : test
#ANALY_INFN-T1_GPU : brokeroff
#ANALY_MANC_GPU : online
#ANALY_OU_OSCER_GPU_TEST : test
#ANALY_QMUL_GPU : test
#ANALY_SLAC_GPU : online
#GOOGLE_GPU : offline
